window.onload = function() {
	var canvas = document.getElementById("canvas"), context = canvas.getContext("2d");

	canvas.width = window.outerWidth;
	canvas.height = window.outerHeight;

	context.fillStyle = "black";
	context.fillRect(0,0,canvas.width,canvas.height);

	var imageObj = document.getElementById("cloud");
	//context.drawImage(imageObject, posX, posY, width, height);
	var posX=0, posY=0;
		setInterval(function() {
			context.fillStyle = "black";
			context.fillRect(0,0,canvas.width,canvas.height);
		
			posX += 1;
			if(posX<canvas.width) {
				context.drawImage(imageObj, posX,50, 200, 130);
				context.drawImage(imageObj, 80+posX,70, 200, 130);
				context.drawImage(imageObj, 140+posX,50, 200, 130);
			}else {
				posX = 0;
			}
		},20);
	
}


